var indexSectionsWithContent =
{
  0: "_abcdefghlmnopqrstuwz",
  1: "cdemqs",
  2: "s",
  3: "cdehlmstu",
  4: "_acdefgmnoprsuwz",
  5: "abcdegmprstwz",
  6: "hlr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

